/*global google, jQuery*/
'use strict';
jQuery(document).ready(function() {
    jQuery('#fullpage').fullpage({
        anchors: ['accueil', 'nosprestations', 'boutique', 'nosautos', 'contact'],
        sectionsColor: ['#FFFFFF', '#F2F2F2', '#eeeeee', '#CBD4DD', '#FFFFFF'],
        css3: true,
        responsive: 768,
        paddingTop: '77px',
        paddingBottom: 0,
        fixedElements: '#header',
        verticalCentered: false,
        autoScrolling: false
    });

    jQuery('#title').animate({ 'top': '+=10px' }, 'slow' );

var map;

function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 45.846223, lng: 1.292385},
        zoom: 16,
        styles: [{'featureType': 'administrative', 'elementType': 'all', 'stylers': [{'visibility': 'on'}, {'lightness': 33}]}, {'featureType': 'landscape', 'elementType': 'all', 'stylers': [{'color': '#f2e5d4'}]}, {'featureType': 'poi.park', 'elementType': 'geometry', 'stylers': [{'color': '#c5dac6'}]}, {'featureType': 'poi.park', 'elementType': 'labels', 'stylers': [{'visibility': 'on'}, {'lightness': 20}]}, {'featureType': 'road', 'elementType': 'all', 'stylers': [{'lightness': 20}]}, {'featureType': 'road.highway', 'elementType': 'geometry', 'stylers': [{'color': '#c5c6c6'}]}, {'featureType': 'road.arterial', 'elementType': 'geometry', 'stylers': [{'color': '#e4d7c6'}]}, {'featureType': 'road.local', 'elementType': 'geometry', 'stylers': [{'color': '#fbfaf7'}]}, {'featureType': 'water', 'elementType': 'all', 'stylers': [{'visibility': 'on'}, {'color': '#acbcc9'}]}]
    });

  var marker = new google.maps.Marker({
    position: {lat: 45.846223, lng: 1.292385},
    title: 'Hello World!',
    map: map,
    icon: 'images/marker.png'
  });
  marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initMap);
});
